﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate);

        Task<T> GetByIdAsync(Guid id);

        Task CreateNew(T item);

        Task<bool> DeleteAsync(Guid id);

        Task<bool> EditAsync(T value);
    }
}