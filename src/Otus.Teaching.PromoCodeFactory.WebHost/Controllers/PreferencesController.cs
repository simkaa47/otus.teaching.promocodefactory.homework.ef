﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Extentions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    /// [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController:ControllerBase
    {
        private readonly IRepository<Preference> _referenceRepo;

        public PreferencesController(IRepository<Preference> referenceRepo)
        {
            _referenceRepo = referenceRepo;
        }

        /// <summary>
        /// Возврат списка предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponce>>> GetAllReferencesAsync()
        {
            var preferences = await _referenceRepo.GetAllAsync();
            var responces = preferences.Select(pref => pref.ToResponce()).ToList();
            return Ok(await _referenceRepo.GetAllAsync());
        }
    }
}
