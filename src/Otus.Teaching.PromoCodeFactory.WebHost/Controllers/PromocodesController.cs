﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Extentions;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase

        
    {
        private readonly IRepository<PromoCode> _promocodeRepo;
        private readonly IRepository<Customer> _customRepo;
        private readonly IRepository<Preference> _prefRepo;

        public PromocodesController(IRepository<PromoCode> promocodeRepo, IRepository<Customer> customRepo, IRepository<Preference> prefRepo)
        {
            _promocodeRepo = promocodeRepo;
            _customRepo = customRepo;
            _prefRepo = prefRepo;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promocodeRepo.GetAllAsync();
            var responce = promoCodes.Select(p => p.ToPromoCodeShortResponse());
            return Ok(responce);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preferences = await _prefRepo.GetWhere(cp => cp.Name == request.Preference);
            if (preferences.Count() != 0)
            {
                var neededCustomers = await _customRepo
                    .GetWhere(c => c.CustomerPreferences
                    .Where(cp => cp.Preference.Name == request.Preference)
                    .FirstOrDefault() != null);
                foreach (var customer in neededCustomers)
                {
                    var promocode = request.ToPromocode(customer, preferences.First());
                    await _promocodeRepo.CreateNew(promocode);
                }
                return Ok();
            }
            return BadRequest();
        }
    }
}