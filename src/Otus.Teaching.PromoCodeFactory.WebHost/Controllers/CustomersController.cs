﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepo;        
        
        public CustomersController(IRepository<Customer> repository, IRepository<PromoCode> promocodeRepo, IRepository<Preference> preferenceRepo)
        {
            _customerRepo = repository;
        }

        /// <summary>
        /// Метод получения всех клиентов
        /// </summary>
        /// <returns>Список клиентов</returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepo.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получить данные клиента по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepo.GetByIdAsync(id);
            if (customer == null) return NotFound();
            var customerResponce = customer.ToCustomerResponce();
            return Ok(customerResponce);
        }

        /// <summary>
        /// Добавить данные клиента
        /// </summary>
        /// <param name="request">Статус выполнения запроса</param>
        /// <returns></returns>
        [HttpPost]        
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (request == null) return BadRequest();
            await _customerRepo.CreateNew(request.ToCustomer());
            return Ok();
        }

        /// <summary>
        /// Редактирование данных клиента
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <param name="request">Данные клиента</param>
        /// <returns>Статус выполнения запроса</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepo.GetByIdAsync(id);
            if(customer==null)return NotFound();
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.CustomerPreferences = request.PreferenceIds
                .Select(id => new CustomerPreference { CustomerId = customer.Id, PreferenceId = id })
                .ToList();            
            if (await _customerRepo.EditAsync(customer)) return Ok();
            return NotFound();
        }

        /// <summary>
        /// Удаление данных клиента по Id
        /// </summary>
        /// <param name="id">Ключ клиента</param>
        /// <returns>Статус выполнения запроса</returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            if(await _customerRepo.DeleteAsync(id)) return Ok();
            return NotFound();
        }
    }
}