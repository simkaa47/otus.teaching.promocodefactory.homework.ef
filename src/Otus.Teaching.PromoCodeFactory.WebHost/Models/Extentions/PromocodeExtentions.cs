﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Extentions
{
    public static class PromocodeExtentions
    {
        public static PromoCodeShortResponse ToPromoCodeShortResponse(this PromoCode promoCode)
        {
            return new PromoCodeShortResponse
            {
                Id = promoCode.Id,
                Code = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                BeginDate  = promoCode.BeginDate.ToString("G"),
                EndDate = promoCode.EndDate.ToString("G"),
                PartnerName  = promoCode.PartnerName
            };
        }

        public static PromoCode ToPromocode(this GivePromoCodeRequest request, Customer customer = null, Preference preference = null)
        {
            return new PromoCode
            {
                Id = Guid.NewGuid(),
                Code = request.PromoCode,
                Customer = customer,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(30),
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                Preference = preference
            };
        }
    }
}
