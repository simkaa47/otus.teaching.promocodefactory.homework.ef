﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Extentions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public static class CustomerExtentions
    {
        public static CustomerResponse ToCustomerResponce(this Customer customer)
        {
            return new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = customer.PromoCodes == null ? new List<PreferenceResponce>() : customer.CustomerPreferences.Select(cp => new PreferenceResponce {Name=cp.Preference.Name, Id = cp.Preference.Id }).ToList(),
                PromoCodes = customer.PromoCodes==null? null: customer.PromoCodes
                .Select(p => p.ToPromoCodeShortResponse())
                .ToList()
            };
        }

        public static Customer ToCustomer(this CreateOrEditCustomerRequest createOrEdit)
        {
            return createOrEdit.ToCustomer(Guid.NewGuid());            
        }

        public static Customer ToCustomer(this CreateOrEditCustomerRequest createOrEdit, Guid id)
        {            
            return new Customer
            {
                Id = id,
                FirstName = createOrEdit.FirstName,
                LastName = createOrEdit.LastName,
                Email = createOrEdit.Email,
                PromoCodes = new List<PromoCode>(),
                CustomerPreferences = createOrEdit.PreferenceIds
                .Select(p => new CustomerPreference { PreferenceId = p, CustomerId = id })
                .ToList()
            };
        }

    }
}