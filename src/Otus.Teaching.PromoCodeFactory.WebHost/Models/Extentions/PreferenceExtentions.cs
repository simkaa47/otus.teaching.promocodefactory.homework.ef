﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Extentions
{
    public static  class PreferenceExtentions
    {
        public static PreferenceResponce ToResponce(this Preference preference)
        {
            return new PreferenceResponce
            {
                Name = preference.Name,
                Id = preference.Id
            };
        }
    }
}
