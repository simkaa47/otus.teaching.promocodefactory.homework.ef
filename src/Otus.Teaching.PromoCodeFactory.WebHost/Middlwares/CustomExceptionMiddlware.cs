﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Middlwares
{
    public class CustomExceptionMiddlware
    {
        private readonly RequestDelegate _next;

        public CustomExceptionMiddlware(RequestDelegate next)
        {
            _next = next;
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex,
      ILogger<CustomExceptionMiddlware> logger, IWebHostEnvironment webHostEnvironment)
        {
            logger.LogError(ex, ex.Message);
            string errorMessageDetails = string.Empty;
            if (!webHostEnvironment.IsProduction())
            {
                errorMessageDetails = ex.Message;
            }
            else
            {
                errorMessageDetails = "ошибка. Пожалуйста, обратитесь к администратору.";
            }
            var result = JsonConvert.SerializeObject(new { error = errorMessageDetails });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return context.Response.WriteAsync(result);
        }
    
    }

    public static class CustomExceptionMiddlwareMiddlewareExtensions
    {
        public static IApplicationBuilder UseCustomExceptionMiddlwareMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CustomExceptionMiddlware>();
        }
    }
}
