﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{

    public class DataContext:DbContext
    {
       
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {            

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<CustomerPreference>()
                .HasKey(cusPref => new { cusPref.CustomerId, cusPref.PreferenceId });
            builder.Entity<CustomerPreference>()
                .HasOne(cusPref => cusPref.Customer)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(cusPref => cusPref.CustomerId);
            builder.Entity<CustomerPreference>()
                .HasOne(cusPref => cusPref.Preference)
                .WithMany()
                .HasForeignKey(cusPref => cusPref.PreferenceId);
        }

       

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Preference> Preferences { get; set; }




    }
}
