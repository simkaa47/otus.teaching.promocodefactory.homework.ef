﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DataContext _dataContext;
        public EfRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await _dataContext.Set<T>().Where(predicate).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await _dataContext
                .Set<T>()
                .ToListAsync();
            return entities;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var entity = await _dataContext
                .Set<T>()
                .FindAsync(id);
            return entity;
        }

        public async Task CreateNew(T item)
        {
            await _dataContext.Set<T>().AddAsync(item);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var removed = await _dataContext.Set<T>().FindAsync(id);
            if (removed == null) return false;
            _dataContext.Set<T>().Remove(removed);
            await _dataContext.SaveChangesAsync();
            return true;
        }

        public async Task<bool> EditAsync(T value)
        {
            _dataContext.Entry(value).State = EntityState.Modified;
            await _dataContext.SaveChangesAsync();
            return true;
        }
    }
}
